package com.ilabservice.darwin.darwincas;

import net.unicon.cas.client.configuration.EnableCasClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableCasClient
@EnableZuulProxy
public class DarwinCasApplication {

	public static void main(String[] args) {
		SpringApplication.run(DarwinCasApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(){ return new RestTemplate();}

}
